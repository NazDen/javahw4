package com.happyfamily;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String habits [];

    void eat(){
        System.out.println("Я кушаю!");
    }
    void respond(){
        System.out.println("Привет, хозяин. Я - "+nickname+". Я соскучился!");
    }
    void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }
    @Override
    public String toString(){
        return (species+"{nickname='"+nickname+"', age="+age+", trickLevel="+trickLevel+", habits="+ Arrays.toString(habits)+"}");
    }
}
