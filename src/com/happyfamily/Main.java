package com.happyfamily;

public class Main {
    public static void main(String[] args) {
    Pet bunny= new Pet();
    bunny.nickname= "Зая";
    bunny.species= "Кролик";
    bunny.trickLevel= 75;
    bunny.age=2;
    bunny.habits= new String[]{"Есть моркуву","Есть капусту"};
    System.out.println(bunny.toString());

    Human katia= new Human();
    katia.name= "Екатерина";
    katia.surname= "Пупкина";
    Human oleg= new Human();
    oleg.name= "Олег";
    oleg.surname= "Пупкин";

    Human vasia=new Human();
    vasia.name= "Вася";
    vasia.surname= "Пупкин";
    vasia.year=1977;
    vasia.iq=80;
    vasia.pet=bunny;
    vasia.mother=katia;
    vasia.father=oleg;
    vasia.greetPet();
    vasia.describePet();
    System.out.println(vasia.toString());
    vasia.feedPet(false);



    }
}
